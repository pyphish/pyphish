# Pyphish

Phishing detection school project.

# Setup

## Dataset initialization

Pyphish dataset needs to be initialized before the entire module can be used.
For now, Pyphish offers a manual way to do so by running `python
src/pyphish/core/__main__.py`.
