"""
pypish is a phishing websites detection utility that implements three
approaches.
1. Whitelist of legitimate websites.
2. Blacklist of phishing websites.
3. Machine learning-based binary classification.
"""

__version__ = "0.0.1"
