from typing import Callable
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
from pyphish.core.sample import Sample
from pyphish.core.features.encoded_exploit import encoded_exploit as f1
from pyphish.core.features.ip_address_explosure import ip_address_explosure as f2
from pyphish.core.features.shortened_url import shortened_url as f3
from pyphish.core.features.amount_of_separators import amount_of_separators as f4
from pyphish.core.features.url_with_variable import url_with_variable as f5
from pyphish.core.features.http_with_specification_port import (
    http_with_specification_port as f6,
)
from pyphish.core.features.url_size import url_size as f7
from pyphish.core.features.nb_sub_domain import nb_sub_domain as f8
from pyphish.core.features.tunneling_protocol import tunneling_protocol as f10
from pyphish.core.features.url_with_redirection import url_with_redirection as f11
from pyphish.core.features.homographic_attack import homographic_attack as f12

from pyphish.core.lists import Whitelist, Blacklist

# from sklearn.metrics import accuracy_score

PYPHISH_FEATURES_SET = "data/pyphish_features_set.csv"
PYPHISH_WHITELIST = "data/pyphish_whitelist.csv"
PYPHISH_BLACKLIST = "data/pyphish_blacklist.csv"

all_features: dict[str, Callable[[Sample], int]] = {
    f1.__name__: f1,
    f2.__name__: f2,
    f3.__name__: f3,
    f4.__name__: f4,
    f5.__name__: f5,
    f6.__name__: f6,
    f7.__name__: f7,
    f8.__name__: f8,
    f10.__name__: f10,
    f11.__name__: f11,
    f12.__name__: f12,
}

enabled_features = all_features

# No longer used.
# dataset = DataSet([pyphish_loader])

features_set = pd.read_csv(PYPHISH_FEATURES_SET)

X_train, X_test, y_train, y_test = train_test_split(
    features_set.iloc[:, 1:-1],
    features_set.iloc[:, -1],
    test_size=0.33,
    random_state=42,
    shuffle=True,
)

model = RandomForestClassifier(n_estimators=100, max_depth=8)
model.fit(X_train, y_train)
# print("Accuracy of train:",accuracy_score(y_train, model.predict(X_train)))
# print("Accuracy of test:",accuracy_score(y_test,model.predict(X_test)))

blacklist = Blacklist(PYPHISH_BLACKLIST)
whitelist = Whitelist(PYPHISH_WHITELIST)


def predict(url: str) -> Sample:
    """
    Predicts whether the sample is a phishing URL or a legitimate one.
    Do note that predict may not be the best descriptive name for this
    function. It should be noted that the function will do the following.
    1. Whitelist check.
    2. Blacklist check.
    3. Random forest check.
    """
    sample = Sample(url)

    if whitelist.contains(sample.url):
        sample.phishing = False
        return sample

    if blacklist.contains(sample.url):
        sample.phishing = True
        return sample

    sample.apply_features(list(enabled_features.values())[1:])
    features_array = np.array(sample.applied_features).reshape(1, -1)
    sample.phishing = bool(model.predict(features_array)[0])
    return sample
