from pyphish.core.loaders import loaders as data_loaders
from pyphish.core.dataset import DataSet
from pyphish.core import enabled_features
from pyphish.core import PYPHISH_FEATURES_SET, PYPHISH_WHITELIST, PYPHISH_BLACKLIST

dataset = DataSet(data_loaders)
dataset.apply_features(list(enabled_features.values()))
dataset.save_csv(PYPHISH_FEATURES_SET, PYPHISH_WHITELIST, PYPHISH_BLACKLIST)

print("{} updated.".format(PYPHISH_FEATURES_SET))
print("{} updated.".format(PYPHISH_WHITELIST))
print("{} updated.".format(PYPHISH_BLACKLIST))
