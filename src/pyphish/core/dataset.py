"""
Dataset contains two types of URLs, legitimate and phishing ones.
See files under data/ for more insight.
"""

import csv
from typing import Callable, List
from pyphish.core.sample import Sample


class DataSet:
    """
    Dataset contains two types of URLs, legitimate and phishing ones.
    """

    def __init__(self, data_loaders: List[Callable[[], List[Sample]]]):
        self.phishing_urls: List[Sample] = []
        self.legitimate_urls: List[Sample] = []
        for loader in data_loaders:
            data = loader()
            for sample in data:
                self.add_sample(sample)
        # XXX This might not be the best way to balance data but it works for
        # now.
        legit_len = len(self.legitimate_urls)
        phish_len = len(self.phishing_urls)
        if legit_len > phish_len:
            self.legitimate_urls = self.legitimate_urls[:phish_len]
        else:
            self.phishing_urls = self.phishing_urls[:legit_len]

    def add_sample(self, sample: Sample):
        """Adds a sample to the dataset."""
        if sample.phishing:
            if sample not in self.phishing_urls:
                self.phishing_urls.append(sample)
        else:
            if sample not in self.legitimate_urls:
                self.legitimate_urls.append(sample)

    def get_phishing(self) -> List[Sample]:
        """Returns phishing samples."""
        return self.phishing_urls

    def get_phishing_netlocs(self) -> List[str]:
        """Returns phishing network locations."""
        results = []
        for sample in self.phishing_urls:
            if sample.parsed_url.netloc not in results:
                results.append(sample.parsed_url.netloc)
        return results

    def get_legitimate(self) -> List[Sample]:
        """Returns legitimate samples."""
        return self.legitimate_urls

    def get_legitimate_netlocs(self) -> List[str]:
        """Returns Legitimate network locations."""
        results = []
        for sample in self.legitimate_urls:
            if sample.parsed_url.netloc not in results:
                results.append(sample.parsed_url.netloc)
        return results

    def get_all(self) -> List[Sample]:
        """Returns all samples."""
        return self.legitimate_urls + self.phishing_urls

    def get_features_labeled(self) -> List[List[int]]:
        """Returns all features without the URLs."""
        results = []
        for sample in self.get_all():
            features = sample.get_applied_features()
            features.append(1 if sample.phishing else 0)
            results.append(features)
        return results

    def apply_features(self, features: List[Callable[[Sample], int]]):
        """Apply given features to all samples of the dataset."""
        for sample in self.get_all():
            sample.apply_features(features)

    def as_list(self) -> List:
        """Returns the entire dataset as a list of lists."""
        results = []
        for sample in self.get_all():
            results.append(sample.as_list())
        return results

    def save_blacklist_csv(self, path: str):
        """Saves blacklist in a CSV file. File is always overwritten."""
        with open(path, "w", encoding="utf-8", newline="") as csvfile:
            writer = csv.writer(csvfile)
            for netloc in self.get_phishing_netlocs():
                writer.writerow([netloc])

    def save_whitelist_csv(self, path: str):
        """Saves whitelist in a CSV file. File is always overwritten."""
        with open(path, "w", encoding="utf-8", newline="") as csvfile:
            writer = csv.writer(csvfile)
            for netloc in self.get_legitimate_netlocs():
                writer.writerow([netloc])

    def save_features_csv(self, path: str):
        """
        Saves features and the result label in a CSV file. File is always
        overwritten.
        """
        with open(path, "w", encoding="utf-8", newline="") as csvfile:
            writer = csv.writer(csvfile)
            for feature in self.get_features_labeled():
                writer.writerow(feature)

    def save_csv(self, features: str, whitelist: str, blacklist: str):
        """Overwrites the given file if it exists already."""
        # self.save_features_csv(features)
        self.save_whitelist_csv(whitelist)
        self.save_blacklist_csv(blacklist)
