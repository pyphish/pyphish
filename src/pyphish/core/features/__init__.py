from pyphish.core.features.encoded_exploit import encoded_exploit
from pyphish.core.features.ip_address_explosure import ip_address_explosure
from pyphish.core.features.shortened_url import shortened_url
from pyphish.core.features.amount_of_separators import amount_of_separators
from pyphish.core.features.url_with_variable import url_with_variable
from pyphish.core.features.http_with_specification_port import (
    http_with_specification_port,
)
from pyphish.core.features.url_with_variable import url_with_variable
from pyphish.core.features.url_size import url_size
from pyphish.core.features.nb_sub_domain import nb_sub_domain
from pyphish.core.features.tunneling_protocol import tunneling_protocol
from pyphish.core.features.url_with_redirection import url_with_redirection
from pyphish.core.features.homographic_attack import homographic_attack
from pyphish.core.features.url_with_variable import url_with_variable
from pyphish.core.features.http_with_specification_port import (
    http_with_specification_port,
)
