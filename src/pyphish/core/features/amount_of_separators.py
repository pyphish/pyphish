from pyphish.core.sample import Sample


def amount_of_separators(sample: Sample) -> int:

    url_part02 = sample.parsed_url._replace(scheme="", netloc="")
    url_part02 = url_part02.geturl()
    return url_part02.count("-")
