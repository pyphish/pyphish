import csv
from pyphish.core.sample import Sample

domainename = []
# use encoding UTF8 because the file qnt lot of encoding types ...
# add newline comme separteur and
with open("data/tld.csv", newline="", encoding="utf-8") as csvfile:
    reader = csv.reader(csvfile, delimiter=" ")
    for row in reader:
        domainename.append(row[0].lower())


def homographic_attack(sample: Sample) -> int:
    domains = sample.parsed_url.netloc.split(".")
    bl = True
    i = 1
    while i <= len(domains) - 1 and bl == True:
        if domains[i].lower() in domainename:
            bl = True
        else:
            bl = False
        i += 1
    return 1 if bl else 0
