from pyphish.core.sample import Sample


def http_with_specification_port(sample: Sample) -> int:
    if sample.parsed_url.scheme.upper() == "HTTP":
        if sample.parsed_url.port != 80 and sample.parsed_url.port is not None:
            return 1
    elif sample.parsed_url.scheme.upper() == "HTTPS":
        if sample.parsed_url.port != 443 and sample.parsed_url.port is not None:
            return 1
    return 0
