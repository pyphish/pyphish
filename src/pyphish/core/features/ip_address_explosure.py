from ipaddress import ip_address
from pyphish.core.sample import Sample


def ip_address_explosure(sample: Sample) -> int:
    try:
        ip_address(sample.parsed_url.netloc)
        return 1
    except ValueError:
        return 0
