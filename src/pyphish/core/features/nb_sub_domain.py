import tldextract
from pyphish.core.sample import Sample


def nb_sub_domain(sample: Sample) -> int:
    url = sample.url
    if "www." in url:
        url = url.replace("www.", "", 1)
    ext = tldextract.extract(url)
    subdomain = ext.subdomain.split(".")
    if "" in subdomain:
        nbsub = 0
    else:
        nbsub = len(subdomain)
    return nbsub
