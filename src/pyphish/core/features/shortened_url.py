import csv
from pyphish.core.sample import Sample

shorteners = []
with open("data/shorteners.csv", encoding="utf-8", newline="") as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        shorteners.append(row[0])


def shortened_url(sample: Sample) -> int:
    return 1 if sample.parsed_url.netloc in shorteners else 0
