from pyphish.core.sample import Sample


def tunneling_protocol(sample: Sample) -> int:
    return 0 if sample.parsed_url.scheme == "https" else 1
