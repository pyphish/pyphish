from pyphish.core.sample import Sample


def url_size(sample: Sample) -> int:
    return len(sample.url)
