from pyphish.core.sample import Sample


def url_with_redirection(sample: Sample) -> int:
    url = sample.parsed_url
    # r = requests.get(url)
    path = url.path
    query = url.query

    if ("//" in path) or (("//") in query):

        return 1
    else:
        return 0


"""      
<<<<  Just a comment to help  >>>>

from urllib.parse import urlparse
url = 'http://netloc/path;param?query=arg#frag' 
o = urlparse(url) 
print(o) 
ParseResult(scheme='http', netloc='netloc', path='/path', params='param', query='query=arg', fragment='frag')

 print(o.path) 
/path
 print(o.query)  
query=arg


o._replace(scheme="",netloc="") 
ParseResult(scheme='', netloc='', path='/path', params='param', query='query=arg', fragment='frag')
print(o.geturl()) 
http://netloc/path;param?query=arg#frag
v=o._replace(scheme="",netloc="") 
print(v.geturl()) 
/path;param?query=arg#frag



url2 = 'https://forecast.weather.gov/MapClick.php?lat=47.19ZPY'                                               
r = requests.get(url2) 
r.status_code
200
r.history    
[]

"""
