from pyphish.core.sample import Sample


def url_with_variable(sample: Sample) -> int:
    splited_url = sample.parsed_url.path.split("/")
    splited_url[:] = (value for value in splited_url if value != "")
    return len(splited_url)
