import csv
import os
from typing import List
import tldextract


class Whitelist:
    """List containing legitimate network locations."""

    def __init__(self, path: str):
        if not os.path.exists(path):
            open(path, "w").close()

        self.data: List[str] = []
        with open(path, encoding="utf-8", newline="") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                self.data.append(row[0])

    def contains(self, url: str) -> bool:
        """Returns whether the given URL is whitelisted or not."""
        parsed_url = tldextract.extract(url)
        return parsed_url.fqdn in self.data


class Blacklist(Whitelist):
    """List containing phishing network locations."""

    def contains(self, url: str) -> bool:
        """
        Returns whether the given URL is whitelisted or not.

        If the smallest domain part is a phishing URL then all descendant are
        phishing URLs as well.
        """
        parsed_url = tldextract.extract(url)
        subdomains = parsed_url.subdomain.split(".")
        netlocs = [f"{parsed_url.domain}.{parsed_url.suffix}"]
        subdomains_len = len(subdomains)

        if "" not in subdomains:
            for i, _ in enumerate(subdomains):
                netloc = ".".join(subdomains[subdomains_len - i - 1 :])
                netloc = f"{netloc}.{netlocs[0]}"
                netlocs.append(netloc)

        for netloc in netlocs:
            if netloc in self.data:
                return True
        return False
