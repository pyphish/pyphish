from typing import Callable, List
from pyphish.core.loaders.phishtank import phishtank_loader
from pyphish.core.loaders.pyphish import pyphish_loader
from pyphish.core.loaders.url_2016 import url_2016_loader
from pyphish.core.sample import Sample

loaders: List[Callable[[], List[Sample]]] = [phishtank_loader, url_2016_loader]
