"""See data/README."""

import csv
from typing import List
from pyphish.core.sample import Sample


def phishtank_loader() -> List[Sample]:
    """ Loads data from phishtank dataset."""
    phishtank_csv = "data/phishtank_verified_online.csv"
    results: List[Sample] = []
    with open(phishtank_csv, encoding="utf-8", newline="") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            results.append(Sample(row[1], True))
    return results
