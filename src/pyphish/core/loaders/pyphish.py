"""See data/README."""

import csv
from typing import List
from pyphish.core.sample import Sample
import os


def pyphish_loader() -> List[Sample]:
    """ Loads data from pyphish dataset."""
    pyphish_csv = "data/pyphish_dataset.csv"
    if not os.path.exists(pyphish_csv):
        open(pyphish_csv, "w").close()
    results: List[Sample] = []
    with open(pyphish_csv, encoding="utf-8", newline="") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            sample = Sample(row[0], row[1] == "1", row[2:])
            results.append(sample)
    return results
