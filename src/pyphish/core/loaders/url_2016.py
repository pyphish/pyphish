"""See data/README."""

import csv
from typing import List
from pyphish.core.sample import Sample


def url_2016_loader() -> List[Sample]:
    """ Loads data from URL 2016 datasets."""
    csv_kind_list = [
        ("data/url_2016_legitimate.csv", False),
        ("data/url_2016_phishing.csv", True),
    ]
    results: List[Sample] = []
    for csv_kind in csv_kind_list:
        with open(csv_kind[0], encoding="utf-8", newline="") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                results.append(Sample(row[0], csv_kind[1]))
    return results
