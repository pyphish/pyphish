"""
Implementation of core classes.
"""

from __future__ import annotations
from urllib.parse import urlparse
from typing import Callable, List


class Sample:
    """
    Data sample implements the intermediary representation of the given URL
    used by the phishing detection approaches.
    """

    def __init__(
        self, url: str, phishing: bool = True, applied_features: List[int] = None
    ):
        self.url = url
        self.parsed_url = urlparse(url)
        self.phishing = phishing
        if applied_features is None:
            self.applied_features = []
        else:
            self.applied_features = applied_features

    def __repr__(self):
        return 'Sample("{}", {})'.format(self.url, self.phishing)

    def get_applied_features(self) -> List[int]:
        """Return applied features list."""
        return self.applied_features

    def apply_features(self, features: List[Callable[[Sample], int]]):
        """Apply given features to the sample."""
        self.applied_features = []
        for feature in features:
            self.applied_features.append(feature(self))

    def as_list(self) -> List:
        """Returns the given sample as a list."""
        res = [self.url, 1 if self.phishing else 0]
        res.extend(self.applied_features)
        return res
