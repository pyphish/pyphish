import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.amount_of_separators import amount_of_separators


def test_genuine_url():
    """Genuine URL."""
    url = "http://www.example.com/additional/stuff"
    sample = Sample(url, False)
    assert amount_of_separators(sample) == 0


def test_amount_of_separators():
    """amount of separators URL."""

    url = "http://www.example.com/additional/stuff-Qiw-Q-123-asd-87-e-ew-w-e-w"
    sample = Sample(url, True)
    assert amount_of_separators(sample) == 10
