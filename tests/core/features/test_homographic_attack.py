import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.homographic_attack import homographic_attack


def test_normal_url():
    """no homographic_attack URL."""
    url = "http://www.google.com/additional/stuff"
    sample = Sample(url, False)
    assert homographic_attack(sample) == 1


def test_homographic_attack():
    """homographic_attack URL."""
    url = "http://www.goole.ZD/additional/stuff"
    sample = Sample(url, False)
    assert homographic_attack(sample) == 0
