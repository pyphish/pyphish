import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.http_with_specification_port import (
    http_with_specification_port,
)


def test_genuine_url():
    """Genuine URL."""
    urls = [
        "http://www.example.com:80/additional/stuff",
        "http://www.example.com/additional/stuff",
        "https://www.example.com:443/additional/stuff",
        "https://www.example.com/additional/stuff",
    ]
    for url in urls:
        sample = Sample(url, True)
        assert http_with_specification_port(sample) == 0


def test_http_with_specification_port_URL():
    """http_with_specification_port_URL."""
    urls = [
        "http://www.example.com:82/additional/stuff",
        "https://www.example.com:4444/additional/stuff",
    ]
    for url in urls:
        sample = Sample(url, True)
        assert http_with_specification_port(sample) == 1
