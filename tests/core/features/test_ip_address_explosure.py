import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.ip_address_explosure import ip_address_explosure


def test_hostname_url():
    """URL with hostname"""
    url = "http://localhost/additional/stuff"
    sample = Sample(url, False)
    assert ip_address_explosure(sample) == 0


def test_ip_address_url():
    """URL with IP address."""
    url = "http://192.168.1.1/additional/stuff"
    sample = Sample(url, False)
    assert ip_address_explosure(sample) == 1
