from pyphish.core.sample import Sample
from pyphish.core.features.nb_sub_domain import nb_sub_domain


def test_url_without_sub_domain():
    """URL without sub-domain"""
    url = "https://www.google.com/"
    sample = Sample(url, False)
    assert nb_sub_domain(sample) == 0


def test_url_with_one_sub_domain():
    """URL with one sub-domain"""
    url = "https://www.sub1.google.com/"
    sample = Sample(url, False)
    assert nb_sub_domain(sample) == 1


def test_url_with_nb_sub_domain_gt_one():
    """URL with more than one sub-domain"""
    url = "https://www.sub1.sub2.google.com/"
    sample = Sample(url, False)
    assert nb_sub_domain(sample) == 2
