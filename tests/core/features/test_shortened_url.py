import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.shortened_url import shortened_url


def test_genuine_url():
    """Genuine URL."""
    url = "http://www.example.com/additional/stuff"
    sample = Sample(url, False)
    assert shortened_url(sample) == 0


def test_shortened_url():
    """Shortened URL."""
    shorteners = [
        "bit.ly",
        "goo.gl",
        "bit.do",
        "ow.ly",
        "tiny.cc",
        "snip.ly",
        "is.gd",
        "u.to",
        "moe.sc",
        "rrd.me",
        "tr.im",
        "ouo.io",
    ]

    for short in shorteners:
        url = "http://" + short + "/xdW3"
        sample = Sample(url, False)
        print(sample.parsed_url)
        assert shortened_url(sample) == 1
