import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.tunneling_protocol import tunneling_protocol


def test_tunneling_protocol():
    """URL with https """
    url = "https://localhost/additional/stuff"
    sample = Sample(url, False)
    assert tunneling_protocol(sample) == 0


def test_No_tunneling_protocol():
    """URL with http """
    url = "http://localhost/additional/stuff"
    sample = Sample(url, False)
    assert tunneling_protocol(sample) == 1
