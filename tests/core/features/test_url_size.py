from pyphish.core.sample import Sample
from pyphish.core.features.url_size import url_size


def test_sizeof_benign_url():
    """size of normal url"""
    url = "http://www.ansi.okstate.edu/breeds/cattle/"
    sample = Sample(url, False)
    assert url_size(sample) == 42


def test_sizeof_phishing_url():
    """size of long url"""
    url = "http://www.easymarketing.hu/wp-includes/images/paypal/paypal/us/error_login.html"
    sample = Sample(url, False)
    assert url_size(sample) == 80
