import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.url_with_redirection import url_with_redirection


def test_url_with_redirection():
    """URL with https """
    url = "https://localhost/additional/stuff"
    sample = Sample(url, False)
    assert url_with_redirection(sample) == 0


def test_no_url_with_redirection():
    """URL with http """
    url = "https://localhost/additional//Go_to_other/site.dz"
    sample = Sample(url, False)
    assert url_with_redirection(sample) == 1
