import pytest
from pyphish.core.sample import Sample
from pyphish.core.features.url_with_variable import url_with_variable


def test_genuine_url():
    """Genuine URL."""
    url = "http://www.example.com/additional/stuff"
    sample = Sample(url, False)
    assert url_with_variable(sample) == 2


def test_url_with_variable():
    """URL with variable ."""

    url = "http://www.example.com/additional/stuff-/stuff2/stuff3/rewj"
    sample = Sample(url, True)
    assert url_with_variable(sample) == 5
