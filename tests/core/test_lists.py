import pytest
from pyphish.core.lists import Blacklist, Whitelist
from pyphish.core import PYPHISH_BLACKLIST, PYPHISH_WHITELIST


def test_blacklist_hits():
    """
    URLs whose network location or parent network location exist in the
    blacklist.
    """
    urls = [
        "chat-by-orkut2.blogspot.co.uk",
        "yahooveriificatioon.weebly.com",
        "www.aimazon.co-jp-qtfr8du0l9s7.buzz",
        "ecrypteddocuments.000webhostapp.com",
        "fb-founders-1994545233616789663-hk.tk",
        "amazoe.buzz",
        "login.amazoe.buzz",
    ]
    blacklist = Blacklist(PYPHISH_BLACKLIST)
    for url in urls:
        assert blacklist.contains(url)


def test_blacklist_misses():
    """
    URLs whose network location or parent network location do not exist in the
    blacklist.
    """
    urls = [
        "blogspot.co.uk",
        "renken.blogspot.co.uk",
        "renken.weebly.com",
        "weebly.com",
        "renken.blogspot.com",
        "www.aimazon.co-jp.buzz",
    ]
    blacklist = Blacklist(PYPHISH_BLACKLIST)
    for url in urls:
        assert not blacklist.contains(url)


def test_whitelist_hits():
    """
    URLs whose network location or parent network location exist in the
    whitelist.
    """
    urls = [
        "askubuntu.com",
        "slashdot.org",
        "medium.com",
        "arstechnica.com",
        "sourceforge.net",
    ]
    whitelist = Whitelist(PYPHISH_WHITELIST)
    for url in urls:
        assert whitelist.contains(url)


def test_whitelist_misses():
    """
    URLs whose network location or parent network location do not exist in the
    whitelist.
    """
    urls = [
        "fb-founders-1994545233616789659-hk.tk",
        "revolution-admin-100002030038264578123431.tk",
        "www.amazon.co-jp.tkloeptr.top",
    ]
    whitelist = Whitelist(PYPHISH_WHITELIST)
    for url in urls:
        assert not whitelist.contains(url)
