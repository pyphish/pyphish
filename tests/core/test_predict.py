import pytest
from pyphish.core import predict


def test_blacklist():
    """Blacklisted network locations."""
    urls = ["chat-by-orkut2.blogspot.co.uk"]
    for url in urls:
        sample = predict(url)
        assert sample.phishing


def test_whitelist():
    """Whitelisted network locations."""
    urls = ["slashdot.org"]
    for url in urls:
        sample = predict(url)
        assert not sample.phishing


def test_ml_legitimate():
    """Legitimate URLs correctly identified by ML."""
    urls = [
        "https://docs.python.org/3/library/concurrent.futures.html",
        "https://gitlab.com/pyphish/pyphish/-/merge_requests/13/diffs",
        "https://www.localhost.com/owo/owo/owo",
    ]
    for url in urls:
        sample = predict(url)
        assert not sample.phishing


def test_ml_phishing():
    """Phishing URLs correctly identified by ML."""
    urls = [
        "http://webmail2.centurytel.net/hwebmail/services/go.php?url=http://63.247.80.138/~julilift/_private/chasesessionsrestore23ghksjf987564jkdgh48937dskjg487395ydjfgsdf84795jhsdf83475sdjfgnmcb47835y/chasesessionsrestore23ghksjf987564jkdgh48937dskjg487395ydjfgsdf84795jhsdf83475sdjfgnmcb47835y/",
    ]
    for url in urls:
        sample = predict(url)
        assert sample.phishing
